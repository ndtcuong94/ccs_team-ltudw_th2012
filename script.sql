USE [CanaryShop]
GO
/****** Object:  Table [dbo].[ADMIN]    Script Date: 6/23/2015 11:58:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ADMIN](
	[UserName] [varchar](50) NOT NULL,
	[PassWord] [varchar](50) NULL,
 CONSTRAINT [PK_ADMIN] PRIMARY KEY CLUSTERED 
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BAIVIET]    Script Date: 6/23/2015 11:58:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BAIVIET](
	[MaBaiViet] [int] NOT NULL,
	[TieuDe] [nvarchar](100) NULL,
	[NoiDung] [nvarchar](400) NULL,
 CONSTRAINT [PK_BAIVIET] PRIMARY KEY CLUSTERED 
(
	[MaBaiViet] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BINHLUAN]    Script Date: 6/23/2015 11:58:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BINHLUAN](
	[Ten] [nvarchar](50) NOT NULL,
	[MaBaiViet] [int] NULL,
	[NoiDung] [nvarchar](200) NULL,
 CONSTRAINT [PK_BINHLUAN] PRIMARY KEY CLUSTERED 
(
	[Ten] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[GIOHANG]    Script Date: 6/23/2015 11:58:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GIOHANG](
	[OrderID] [int] NOT NULL,
	[KhachHang] [nvarchar](50) NULL,
	[Email] [varchar](50) NULL,
	[SDT] [varchar](12) NULL,
	[DiaChi] [nvarchar](50) NULL,
	[TrangThai] [bit] NULL,
 CONSTRAINT [PK_GIOHANG] PRIMARY KEY CLUSTERED 
(
	[OrderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LOAISANPHAM]    Script Date: 6/23/2015 11:58:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LOAISANPHAM](
	[IDLoaiSP] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[NgayTao] [datetime] NULL CONSTRAINT [DF_LOAISANPHAM_NgayTao]  DEFAULT (getdate()),
	[Order] [int] NULL,
	[TrangThai] [bit] NULL,
 CONSTRAINT [PK_LOAISANPHAM] PRIMARY KEY CLUSTERED 
(
	[IDLoaiSP] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OrderDetail]    Script Date: 6/23/2015 11:58:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderDetail](
	[ID] [int] NOT NULL,
	[OrderID] [int] NULL,
	[MaSP] [int] NULL,
	[SoLuong] [int] NULL,
 CONSTRAINT [PK_OrderDetail] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SANPHAMQA]    Script Date: 6/23/2015 11:58:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SANPHAMQA](
	[MaSP] [int] NOT NULL,
	[TenSP] [nvarchar](50) NULL,
	[IDLoaiSP] [int] NULL,
	[HinhAnh] [nvarchar](250) NULL,
	[GiaBan] [decimal](18, 0) NULL,
	[NgayTao] [datetime] NULL CONSTRAINT [DF_SANPHAMQA_NgayTao]  DEFAULT (getdate()),
	[ChiTiet] [ntext] NULL,
	[TrangThai] [bit] NULL,
	[Mota] [nvarchar](250) NULL,
 CONSTRAINT [PK_SANPHAMQA] PRIMARY KEY CLUSTERED 
(
	[MaSP] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TAIKHOAN]    Script Date: 6/23/2015 11:58:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TAIKHOAN](
	[MaKH] [int] IDENTITY(1,1) NOT NULL,
	[TenDangNhap] [varchar](50) NULL,
	[MatKhau] [varchar](50) NULL,
	[HoTen] [nvarchar](50) NULL,
	[GioiTinh] [nvarchar](7) NULL,
	[DiaChi] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[SDT] [nvarchar](12) NULL,
 CONSTRAINT [PK_TAIKHOAN] PRIMARY KEY CLUSTERED 
(
	[MaKH] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[ADMIN] ([UserName], [PassWord]) VALUES (N'admin', N'123456')
INSERT [dbo].[GIOHANG] ([OrderID], [KhachHang], [Email], [SDT], [DiaChi], [TrangThai]) VALUES (0, N'a', NULL, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[LOAISANPHAM] ON 

INSERT [dbo].[LOAISANPHAM] ([IDLoaiSP], [Ten], [NgayTao], [Order], [TrangThai]) VALUES (1, N'Áo Nam', CAST(N'2015-06-07 02:35:45.440' AS DateTime), 1, 1)
INSERT [dbo].[LOAISANPHAM] ([IDLoaiSP], [Ten], [NgayTao], [Order], [TrangThai]) VALUES (2, N'Áo Nữ', CAST(N'2015-06-07 02:36:22.920' AS DateTime), 2, 1)
INSERT [dbo].[LOAISANPHAM] ([IDLoaiSP], [Ten], [NgayTao], [Order], [TrangThai]) VALUES (3, N'Áo Trẻ Em', CAST(N'2015-06-07 02:36:44.717' AS DateTime), 5, 0)
INSERT [dbo].[LOAISANPHAM] ([IDLoaiSP], [Ten], [NgayTao], [Order], [TrangThai]) VALUES (4, N'gfgfgfg', CAST(N'2015-06-07 02:42:34.767' AS DateTime), 6, 1)
INSERT [dbo].[LOAISANPHAM] ([IDLoaiSP], [Ten], [NgayTao], [Order], [TrangThai]) VALUES (5, N'Áo Mưa Nam', CAST(N'2015-06-07 13:23:38.790' AS DateTime), 6, 0)
INSERT [dbo].[LOAISANPHAM] ([IDLoaiSP], [Ten], [NgayTao], [Order], [TrangThai]) VALUES (6, N'Áo Mưa Nam', CAST(N'2015-06-07 13:23:44.530' AS DateTime), 6, 1)
INSERT [dbo].[LOAISANPHAM] ([IDLoaiSP], [Ten], [NgayTao], [Order], [TrangThai]) VALUES (7, N'áo chống nắng', CAST(N'2015-06-18 20:16:22.193' AS DateTime), 11, 1)
INSERT [dbo].[LOAISANPHAM] ([IDLoaiSP], [Ten], [NgayTao], [Order], [TrangThai]) VALUES (8, N'Áo chống nắng', CAST(N'2015-06-22 10:06:19.580' AS DateTime), 4, 1)
INSERT [dbo].[LOAISANPHAM] ([IDLoaiSP], [Ten], [NgayTao], [Order], [TrangThai]) VALUES (9, N'Quần Nam', CAST(N'2015-06-22 10:18:44.033' AS DateTime), 8, 1)
INSERT [dbo].[LOAISANPHAM] ([IDLoaiSP], [Ten], [NgayTao], [Order], [TrangThai]) VALUES (10, N'Quần Nữ', CAST(N'2015-06-22 10:29:28.010' AS DateTime), 5, 0)
SET IDENTITY_INSERT [dbo].[LOAISANPHAM] OFF
INSERT [dbo].[OrderDetail] ([ID], [OrderID], [MaSP], [SoLuong]) VALUES (1, 1, 1, 1)
INSERT [dbo].[OrderDetail] ([ID], [OrderID], [MaSP], [SoLuong]) VALUES (2, 2, 1, 3)
INSERT [dbo].[SANPHAMQA] ([MaSP], [TenSP], [IDLoaiSP], [HinhAnh], [GiaBan], [NgayTao], [ChiTiet], [TrangThai], [Mota]) VALUES (1, N'Áo Kimono Nhật', 2, N'/Images/WomanCoat/1_109_104.jpg', CAST(169000 AS Decimal(18, 0)), CAST(N'2015-06-18 16:57:24.177' AS DateTime), N'<div class="tab-pane active" id="home">Sản phẩm hàng hiệu theo phong cách Nhật Bản. Mang lại phong thái sành điệu khi đi ngoài đường; Phù hợp với mọi đối tượng.</div>', NULL, NULL)
INSERT [dbo].[SANPHAMQA] ([MaSP], [TenSP], [IDLoaiSP], [HinhAnh], [GiaBan], [NgayTao], [ChiTiet], [TrangThai], [Mota]) VALUES (2, N'Áo Vest Thời Trang', 2, N'/Images/WomanCoat/1_16_100.jpg', CAST(196000 AS Decimal(18, 0)), CAST(N'2015-06-18 16:57:24.177' AS DateTime), N'<div class="tab-pane active" id="home">Sản phẩm thời trang phong cách theo phong cách Nhật Bản. Mang lại phong thái sành điệu khi đi ngoài đường; Phù hợp với mọi đối tượng.</div>', NULL, NULL)
INSERT [dbo].[SANPHAMQA] ([MaSP], [TenSP], [IDLoaiSP], [HinhAnh], [GiaBan], [NgayTao], [ChiTiet], [TrangThai], [Mota]) VALUES (3, N'Áo Cổ Ngắn', 2, N'/Images/WomanCoat/1_16_100.jpg', CAST(69000 AS Decimal(18, 0)), CAST(N'2015-06-18 16:57:24.177' AS DateTime), N'<div class="tab-pane active" id="home">Sản phẩm mới nhất được nhập khẩu từ Italia</div>', NULL, NULL)
INSERT [dbo].[SANPHAMQA] ([MaSP], [TenSP], [IDLoaiSP], [HinhAnh], [GiaBan], [NgayTao], [ChiTiet], [TrangThai], [Mota]) VALUES (4, N'Áo Cổ Dài', 2, N'/Images/WomanCoat/1_16_364.jpg', CAST(78000 AS Decimal(18, 0)), CAST(N'2015-06-18 16:57:24.177' AS DateTime), N'<div class="tab-pane active" id="home">Sản phẩm phù hợp với mọi lứa tuổi, trẻ trung, năng động</div>', NULL, NULL)
INSERT [dbo].[SANPHAMQA] ([MaSP], [TenSP], [IDLoaiSP], [HinhAnh], [GiaBan], [NgayTao], [ChiTiet], [TrangThai], [Mota]) VALUES (5, N'Áo Sành Điệu', 2, N'/Images/WomanCoat/1_87_1.jpg', CAST(100000 AS Decimal(18, 0)), CAST(N'2015-06-18 16:57:24.177' AS DateTime), N'<div class="tab-pane active" id="home">Sản phẩm là sự kết tinh của nhiều nguyên liệu quý</div>', NULL, NULL)
INSERT [dbo].[SANPHAMQA] ([MaSP], [TenSP], [IDLoaiSP], [HinhAnh], [GiaBan], [NgayTao], [ChiTiet], [TrangThai], [Mota]) VALUES (6, N'Áo mới nhập khẩu ', 2, N'/Images/WomanCoat/ao-khoac-nu-2014_5.jpg', CAST(200000 AS Decimal(18, 0)), CAST(N'2015-06-18 16:57:24.177' AS DateTime), N'<div class="tab-pane active" id="home">Sản phẩm theo phong cách làm việc</div>', NULL, NULL)
INSERT [dbo].[SANPHAMQA] ([MaSP], [TenSP], [IDLoaiSP], [HinhAnh], [GiaBan], [NgayTao], [ChiTiet], [TrangThai], [Mota]) VALUES (7, N'Áo Nữ Thời Trang', 2, N'/Images/WomanCoat/1_43_1.jpg', CAST(50000 AS Decimal(18, 0)), CAST(N'2015-06-18 16:57:24.177' AS DateTime), N'<div class="tab-pane active" id="home">Sản phẩm là sự kết tinh của nhiều nguyên liệu quý</div>', NULL, NULL)
INSERT [dbo].[SANPHAMQA] ([MaSP], [TenSP], [IDLoaiSP], [HinhAnh], [GiaBan], [NgayTao], [ChiTiet], [TrangThai], [Mota]) VALUES (8, N'Hàng Ngoại Nhập', 2, N'/Images/WomanCoat/ao-khoac-vest-nu_3_7.jpg', CAST(400000 AS Decimal(18, 0)), CAST(N'2015-06-18 16:57:24.177' AS DateTime), N'<div class="tab-pane active" id="home">Sản phẩm là sự kết tinh của nhiều nguyên liệu quý</div>', NULL, NULL)
INSERT [dbo].[SANPHAMQA] ([MaSP], [TenSP], [IDLoaiSP], [HinhAnh], [GiaBan], [NgayTao], [ChiTiet], [TrangThai], [Mota]) VALUES (9, N'Áo Chất Lượng Cao', 2, N'/Images/WomanCoat/1_101_325.jpg', CAST(300000 AS Decimal(18, 0)), CAST(N'2015-06-18 16:57:24.177' AS DateTime), N'<div class="tab-pane active" id="home">Sản phẩm theo phong cách làm việc</div>', NULL, NULL)
INSERT [dbo].[SANPHAMQA] ([MaSP], [TenSP], [IDLoaiSP], [HinhAnh], [GiaBan], [NgayTao], [ChiTiet], [TrangThai], [Mota]) VALUES (11, N'Áo Vest Nam', 1, N'/Images/AoNam/2.jpg', CAST(89000 AS Decimal(18, 0)), CAST(N'2015-06-18 16:57:24.177' AS DateTime), N'<div class="tab-pane active" id="home">Sản phẩm theo phong cách làm việc</div>', NULL, NULL)
INSERT [dbo].[SANPHAMQA] ([MaSP], [TenSP], [IDLoaiSP], [HinhAnh], [GiaBan], [NgayTao], [ChiTiet], [TrangThai], [Mota]) VALUES (12, N'Áo Thời Trang', 1, N'/Images/AoNam/3.jpg', CAST(99000 AS Decimal(18, 0)), CAST(N'2015-06-18 16:57:24.177' AS DateTime), N'<div class="tab-pane active" id="home">Sản phẩm hàng hiệu theo phong cách Nhật Bản. Mang lại phong thái sành điệu khi đi ngoài đường; Phù hợp với mọi đối tượng.</div>', NULL, NULL)
INSERT [dbo].[SANPHAMQA] ([MaSP], [TenSP], [IDLoaiSP], [HinhAnh], [GiaBan], [NgayTao], [ChiTiet], [TrangThai], [Mota]) VALUES (13, N'Áo Nam Hàng Hiệu', 1, N' /Images/AoNam/4.jpg', CAST(110000 AS Decimal(18, 0)), CAST(N'2015-06-18 16:57:24.177' AS DateTime), N'<div class="tab-pane active" id="home">Sản phẩm hàng hiệu theo phong cách Nhật Bản. Mang lại phong thái sành điệu khi đi ngoài đường; Phù hợp với mọi đối tượng.</div>', NULL, NULL)
INSERT [dbo].[SANPHAMQA] ([MaSP], [TenSP], [IDLoaiSP], [HinhAnh], [GiaBan], [NgayTao], [ChiTiet], [TrangThai], [Mota]) VALUES (14, N'Áo Nhập Khẩu Từ Canada', 1, N'/Images/AoNam/5.jpg', CAST(189000 AS Decimal(18, 0)), CAST(N'2015-06-18 16:57:24.177' AS DateTime), N'<div class="tab-pane active" id="home">Sản phẩm thời trang phong cách theo phong cách Nhật Bản. Mang lại phong thái sành điệu khi đi ngoài đường; Phù hợp với mọi đối tượng.</div>', NULL, NULL)
INSERT [dbo].[SANPHAMQA] ([MaSP], [TenSP], [IDLoaiSP], [HinhAnh], [GiaBan], [NgayTao], [ChiTiet], [TrangThai], [Mota]) VALUES (15, N'Áo Nam Thời Trang', 1, N'/Images/AoNam/9.jpg', CAST(250000 AS Decimal(18, 0)), CAST(N'2015-06-18 16:57:24.177' AS DateTime), N'<div class="tab-pane active" id="home">Sản phẩm thời trang phong cách theo phong cách Nhật Bản. Mang lại phong thái sành điệu khi đi ngoài đường; Phù hợp với mọi đối tượng.</div>', NULL, NULL)
INSERT [dbo].[SANPHAMQA] ([MaSP], [TenSP], [IDLoaiSP], [HinhAnh], [GiaBan], [NgayTao], [ChiTiet], [TrangThai], [Mota]) VALUES (16, N'Áo Vest Hàng Hiệu', 1, N'/Images/AoNam/7.jpg', CAST(300000 AS Decimal(18, 0)), CAST(N'2015-06-18 16:57:24.177' AS DateTime), N'<div class="tab-pane active" id="home">Sản phẩm phù hợp cho nhân viên văn phòng</div>', NULL, NULL)
INSERT [dbo].[SANPHAMQA] ([MaSP], [TenSP], [IDLoaiSP], [HinhAnh], [GiaBan], [NgayTao], [ChiTiet], [TrangThai], [Mota]) VALUES (17, N'Áo Thời Trang Hè', 1, N'/Images/AoNam/8.jpg', CAST(400000 AS Decimal(18, 0)), CAST(N'2015-06-18 17:02:22.657' AS DateTime), N'<div class="tab-pane active" id="home">Sản phẩm mới nhất được nhập khẩu từ Italia</div>', NULL, NULL)
INSERT [dbo].[SANPHAMQA] ([MaSP], [TenSP], [IDLoaiSP], [HinhAnh], [GiaBan], [NgayTao], [ChiTiet], [TrangThai], [Mota]) VALUES (18, N'Áo Vest Công Sở', 1, N'/Images/AoNam/6.jpg', CAST(50000 AS Decimal(18, 0)), CAST(N'2015-06-18 17:02:48.663' AS DateTime), N'<div class="tab-pane active" id="home">Sản phẩm mới nhất được nhập khẩu từ Italia</div>', NULL, NULL)
SET IDENTITY_INSERT [dbo].[TAIKHOAN] ON 

INSERT [dbo].[TAIKHOAN] ([MaKH], [TenDangNhap], [MatKhau], [HoTen], [GioiTinh], [DiaChi], [Email], [SDT]) VALUES (0, N'Admin', N'123456', N'Phan Văn Công', N'Nam', N'Lao', N'pvcong11031994@gmail.com', N'0969969969')
INSERT [dbo].[TAIKHOAN] ([MaKH], [TenDangNhap], [MatKhau], [HoTen], [GioiTinh], [DiaChi], [Email], [SDT]) VALUES (1, N'pvc', N'123456', N'PVC', N'Nam', N'HN', N'pvcong110319942@gmail.com', N'01652717826')
INSERT [dbo].[TAIKHOAN] ([MaKH], [TenDangNhap], [MatKhau], [HoTen], [GioiTinh], [DiaChi], [Email], [SDT]) VALUES (3, N'nguoidung1', N'123456', N'nguoidung', N'Nam', N'VT', N'a@gmali.com', N'0123456789')
INSERT [dbo].[TAIKHOAN] ([MaKH], [TenDangNhap], [MatKhau], [HoTen], [GioiTinh], [DiaChi], [Email], [SDT]) VALUES (4, N'nqs', N'123456', N'nguoidung2', N'Nam', N'VT', N'b@gmail.com', N'0969969969')
INSERT [dbo].[TAIKHOAN] ([MaKH], [TenDangNhap], [MatKhau], [HoTen], [GioiTinh], [DiaChi], [Email], [SDT]) VALUES (5, N'ndtc', N'123456', N'Cường', N'Nữ', N'hcm', N'a@gmail.com', N'123456')
INSERT [dbo].[TAIKHOAN] ([MaKH], [TenDangNhap], [MatKhau], [HoTen], [GioiTinh], [DiaChi], [Email], [SDT]) VALUES (6, N'nguoidung3', N'123456', N'abc', N'Nữ', N'hcm', N'pvcong110319943@gmail.com', N'0123456789')
INSERT [dbo].[TAIKHOAN] ([MaKH], [TenDangNhap], [MatKhau], [HoTen], [GioiTinh], [DiaChi], [Email], [SDT]) VALUES (1005, N'nguoidung8', N'12345678', N'abc', N'Nam', N'hcm', N'pvcong110319943@gmail.com', N'0123456789')
INSERT [dbo].[TAIKHOAN] ([MaKH], [TenDangNhap], [MatKhau], [HoTen], [GioiTinh], [DiaChi], [Email], [SDT]) VALUES (1006, N'nguoidung8', N'12345678', N'fdf', N'Nam', N'fdf', N'fd@gmail.com', N'fd')
INSERT [dbo].[TAIKHOAN] ([MaKH], [TenDangNhap], [MatKhau], [HoTen], [GioiTinh], [DiaChi], [Email], [SDT]) VALUES (1007, N'Nguoidung19', N'12345678', N'A', N'Nam', N'TPHCM', N'a@gmail.com', N'0963731250')
INSERT [dbo].[TAIKHOAN] ([MaKH], [TenDangNhap], [MatKhau], [HoTen], [GioiTinh], [DiaChi], [Email], [SDT]) VALUES (1008, N'onhunhu', N'qqqqqq111111', N'Cường', N'Nam', N'dklfjdsjfldjslfjs', N'cuong@13081994.com', N'0169696969')
INSERT [dbo].[TAIKHOAN] ([MaKH], [TenDangNhap], [MatKhau], [HoTen], [GioiTinh], [DiaChi], [Email], [SDT]) VALUES (1009, N'abc', N'12345678', N'Cường', N'Nam', N'hcm', N'a@gmail.com', N'0123456789')
INSERT [dbo].[TAIKHOAN] ([MaKH], [TenDangNhap], [MatKhau], [HoTen], [GioiTinh], [DiaChi], [Email], [SDT]) VALUES (1010, N'nguoidung8999', N'12345678', N'abc', N'Nam', N'hvm', N'a@gmail.com', N'0123456789')
INSERT [dbo].[TAIKHOAN] ([MaKH], [TenDangNhap], [MatKhau], [HoTen], [GioiTinh], [DiaChi], [Email], [SDT]) VALUES (1011, N'qwerty', N'123456789', N'abc', N'Nam', N'hcm', N'a@gmail.com', N'0123456789')
INSERT [dbo].[TAIKHOAN] ([MaKH], [TenDangNhap], [MatKhau], [HoTen], [GioiTinh], [DiaChi], [Email], [SDT]) VALUES (1012, N'abc', N'12345678', N'abc', N'Nữ', N'hcm', N'a@gmail.com', N'0123456789')
SET IDENTITY_INSERT [dbo].[TAIKHOAN] OFF
/****** Object:  StoredProcedure [dbo].[SP_Account_Login]    Script Date: 6/23/2015 11:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[SP_Account_Login]
	@TenDangNhap varchar(50),
	@MatKhau varchar(50)
AS
BEGIN
	DECLARE @count int
	DECLARE @res bit
	select @count = count(*) from ADMIN where UserName = @TenDangNhap and PassWord = @MatKhau
	if @count > 0
		set @res = 1
	else
		set @res = 0
	select @res
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Account_Login_KhachHang]    Script Date: 6/23/2015 11:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[SP_Account_Login_KhachHang]
	@TenDangNhap varchar(50),
	@MatKhau varchar(50)
AS
BEGIN
	DECLARE @count int
	DECLARE @res bit
	select @count = count(*) from TAIKHOAN where TenDangNhap = @TenDangNhap and MatKhau = @MatKhau
	if @count > 0
		set @res = 1
	else
		set @res = 0
	select @res
END
GO
/****** Object:  StoredProcedure [dbo].[Sp_LoaiSanPham_Insert]    Script Date: 6/23/2015 11:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_LoaiSanPham_Insert]
	@Ten nvarchar(50),
	@Order int,
	@TrangThai bit
AS
BEGIN
	insert into LOAISANPHAM(Ten,NgayTao,[Order],TrangThai)
	values(@Ten,getdate(),@Order,@TrangThai)
END

GO
/****** Object:  StoredProcedure [dbo].[Sp_LoaiSanPham_ListAll]    Script Date: 6/23/2015 11:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Sp_LoaiSanPham_ListAll]
as
select * from LOAISANPHAM where  TrangThai = 1 or TrangThai = 0
order by [Order] asc
GO
/****** Object:  StoredProcedure [dbo].[sp_User_Delete]    Script Date: 6/23/2015 11:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[sp_User_Delete]
@MaKH int
As
Delete from [TAIKHOAN] where MaKH=@MaKH
GO
/****** Object:  StoredProcedure [dbo].[sp_User_GetByAll]    Script Date: 6/23/2015 11:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[sp_User_GetByAll]
As
Select * From [TAIKHOAN]
GO
/****** Object:  StoredProcedure [dbo].[sp_User_GetByMaKH]    Script Date: 6/23/2015 11:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[sp_User_GetByMaKH]
@MaKH int
As
Select * From [TAIKHOAN] Where MaKH = @MaKH
GO
/****** Object:  StoredProcedure [dbo].[sp_User_GetByTop]    Script Date: 6/23/2015 11:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[sp_User_GetByTop]
@Top nvarchar(10),
@Where nvarchar(200),
@Order nvarchar(200)
As
	Declare @SQL As nvarchar(500)
	Select @SQL = 'SELECT TOP ('+@Top+') * FROM [TAIKHOAN]'
	If LEN (@Top) = 0
		Begin
			Select @SQL = 'Select * FROM TAIKHOAN'
		End
	If LEN (@Where) > 0
		Begin
			Select @SQL = @SQL + 'WHERE' + @Where
		End
	If LEN (@Order) > 0
		Begin
			Select @SQL = @SQL + 'ORDER BY' + @Order
		End
GO
/****** Object:  StoredProcedure [dbo].[sp_User_Insert]    Script Date: 6/23/2015 11:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[sp_User_Insert]
@Ten nvarchar(500),
@MatKhau nvarchar(50),
@HoTen nvarchar(50),
@GioiTinh int,
@DiaChi nvarchar(50),
@Email nvarchar(50),
@SDT nvarchar(12)
As
Insert into [TAIKHOAN]([TenDangNhap],[MatKhau],[HoTen],[GioiTinh],[DiaChi],[Email],[SDT]) values (@Ten, @MatKhau, @HoTen, @GioiTinh,@DiaChi, @Email, @SDT)
GO
/****** Object:  StoredProcedure [dbo].[sp_User_Update]    Script Date: 6/23/2015 11:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[sp_User_Update]
@MaKH int,
@Ten nvarchar(500),
@MatKhau nvarchar(50),
@HoTen nvarchar(50),
@GioiTinh int,
@DiaChi nvarchar(50),
@Email nvarchar(50),
@SDT nvarchar(12)
As
Update [TAIKHOAN] Set [TenDangNhap]=@Ten, [MatKhau]=@MatKhau, [HoTen]=@HoTen, [GioiTinh]=@GioiTinh, [DiaChi]=@DiaChi, [Email]=@Email, [SDT]=@SDT where MaKH=@MaKH
GO
