namespace Models.Frameworks
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BAIVIET")]
    public partial class BAIVIET
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MaBaiViet { get; set; }

        [StringLength(100)]
        public string TieuDe { get; set; }

        [StringLength(400)]
        public string NoiDung { get; set; }
    }
}
