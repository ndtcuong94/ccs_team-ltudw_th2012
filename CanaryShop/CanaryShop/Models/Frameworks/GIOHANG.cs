﻿namespace Models.Frameworks
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("GIOHANG")]
    public partial class GIOHANG
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int OrderID { get; set; }

        [StringLength(50)]
        [Display(Name = "Tên Khách Hàng"), Required(ErrorMessage = "Chưa nhập tên")]
        public string KhachHang { get; set; }

        [StringLength(50)]
        [Display(Name = "Email Liên Hệ"), Required(ErrorMessage = "Chưa nhập Email")]
        public string Email { get; set; }

        [StringLength(12)]
        [Display(Name = "Số Điện Thoại"), Required(ErrorMessage = "Chưa nhập số điện thoại")]
        public string SDT { get; set; }

        [StringLength(50)]
        [Display(Name = "Địa chỉ liên hệ")]
        public string DiaChi { get; set; }

        [Display(Name = "Trạng thái đơn hàng")]
        public bool? TrangThai { get; set; }

        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
