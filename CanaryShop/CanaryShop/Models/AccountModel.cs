﻿using Models.Frameworks;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Configuration;
namespace Models
{
    public class AccountModel        
    {

        private OnlineShopDbContext context = null;

        public AccountModel()
        {
            context = new OnlineShopDbContext();
        }

        public bool Login(string userName, string passWord)
        {
            object[] sqlPara=
            {
                new SqlParameter("@UserName", userName),
                new SqlParameter("@PassWord", passWord),
            };
            var res = 
                context.Database.SqlQuery<bool>("SP_Account_Login @UserName, @PassWord", sqlPara).SingleOrDefault();
            return res;
        }


        public bool LoginKH(string userName, string passWord)
        {
            object[] sqlPara =
            {
                new SqlParameter("@UserName", userName),
                new SqlParameter("@PassWord", passWord),
            };
            var res =
                context.Database.SqlQuery<bool>("SP_Account_Login_KhachHang @UserName, @PassWord", sqlPara).SingleOrDefault();
            return res;
        }
    }
}
