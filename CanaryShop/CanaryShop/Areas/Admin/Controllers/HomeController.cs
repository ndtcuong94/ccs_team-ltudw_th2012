﻿using Models;
using Models.Frameworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CanaryShop.Areas.Admin.Controllers
{

    public class HomeController : Controller
    {
        // GET: Admin/Home
        public ActionResult Index()
        {
            if(Session["user"] == null)
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }


        public ActionResult Create()
        {
            return View();
        }
        //Tao 1 tai khoan nguoi dung
        //public ActionResult Create(TAIKHOAN collection)
        //{
        //    try
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            var model = new QLNguoiDungModel();
        //            int res = model.Create(collection.TenDangNhap, collection.MatKhau, collection.HoTen, collection.GioiTinh, collection.DiaChi, collection.Email, collection.SDT);
        //            if (res > 0)
        //            {
        //                return RedirectToAction("Index");
        //            }
        //            else
        //            {
        //                ModelState.AddModelError("", "Them moi that bai.");
        //            }
        //            return View(collection);
        //        }
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}
    }
}