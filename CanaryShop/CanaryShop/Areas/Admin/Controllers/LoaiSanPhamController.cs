﻿using Models;
using Models.Frameworks;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CanaryShop.Areas.Admin.Controllers
{
    public class LoaiSanPhamController : Controller
    {
        private OnlineShopDbContext dbContext = new OnlineShopDbContext();
        // GET: Admin/LoaiSanPham
        public ActionResult Index()
        {
            var implSp = new LoaiSanPhamModel();
            var model = implSp.ListAll();
            return View(model); 
            
        }

        // GET: Admin/LoaiSanPham/Details/5
        public ActionResult Details(int id)
        {
            return View(dbContext.LOAISANPHAMs.First(c=>c.IDLoaiSP == id));
        }

        // GET: Admin/LoaiSanPham/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/LoaiSanPham/Create
        [HttpPost]
        public ActionResult Create(LOAISANPHAM collection)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var model = new LoaiSanPhamModel();
                    int res = model.Create(collection.Ten, collection.Order, collection.TrangThai);
                    if (res > 0)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Thêm mới thất bại.");
                    }              
                }
                
                return View(collection);
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/LoaiSanPham/Edit/5
        [HttpGet]
        public ActionResult Edit(int id)
        {
            return View(dbContext.LOAISANPHAMs.First(c=>c.IDLoaiSP == id));
        }

        // POST: Admin/LoaiSanPham/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, LOAISANPHAM model)
        {
            try
            {
                // TODO: Add update logic here
                if (ModelState.IsValid)
                {
                    LOAISANPHAM tk = dbContext.LOAISANPHAMs.First(c => c.IDLoaiSP == id);
                    ((IObjectContextAdapter)dbContext).ObjectContext.ApplyCurrentValues("LOAISANPHAMs", model);
                    dbContext.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(model);
            }
            catch
            {
                return View();
            }
        }

        // POST: Admin/LoaiSanPham/Delete/5
        [HttpPost]
        public ActionResult Delete(int id)
        {
            var _Category = dbContext.LOAISANPHAMs.Find(id);
            _Category.SANPHAMQAs.ToList().ForEach(m => dbContext.SANPHAMQAs.Remove(m));
            dbContext.LOAISANPHAMs.Remove(_Category);
            dbContext.SaveChanges();
            return new EmptyResult();
        }
    }
}
