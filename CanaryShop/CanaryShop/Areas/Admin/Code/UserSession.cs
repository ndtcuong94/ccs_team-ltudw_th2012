﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CanaryShop.Code
{
    [Serializable] 
    public class UserSession
    {
        public String UserName { get; set; }   
    }
}