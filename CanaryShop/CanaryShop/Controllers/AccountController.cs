﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CanaryShop.Models;
using PagedList;

namespace CanaryShop.Controllers
{
    public class AccountController : Controller
    {
        //
        // GET: /Account/
        public ActionResult Login()
        {
            return View();
        }

        public ActionResult Register()
        {
            return View();
        }

        private CanaryShopEntity CSData = new CanaryShopEntity();
        public ActionResult AccountList(string Sorting_Order, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = Sorting_Order;
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var acc = from s in CSData.TAIKHOANs
                     select s;
            if (!String.IsNullOrEmpty(searchString))
            {
                acc = acc.Where(s => s.Username.Contains(searchString)
                                       || s.Fullname.Contains(searchString) || s.Email.Contains(searchString));
            }
            switch (Sorting_Order)
            {
                case "username":
                    acc = acc.OrderBy(s => s.Username);
                    break;
                case "fullname":
                    acc = acc.OrderBy(s => s.Fullname);
                    break;
                case "email":
                    acc = acc.OrderBy(s => s.Email);
                    break;
                case "dob":
                    acc = acc.OrderBy(s => s.DOB);
                    break;

                default:
                    acc = acc.OrderBy(s => s.Fullname);
                    break;
            }
            int pageSize = 2;
            int pageNumber = (page ?? 1);
            return View(acc.ToPagedList(pageNumber, pageSize));
        }

        public ActionResult DeleteAccount(string ID)
        {
            TAIKHOAN acc = CSData.TAIKHOANs.Single(t => t.Username.Equals(ID));
            CSData.TAIKHOANs.Remove(acc);
            CSData.SaveChanges();
            return RedirectToAction("AccountList");
        }

        public ActionResult Identity(string ID)
        {
            TAIKHOAN acc = CSData.TAIKHOANs.Single(t => t.Username.Equals(ID));
            if (acc.Author == "Người dùng")
            {
                acc.Author = "Admin";
            }
            else
            {
                acc.Author = "Người dùng";
            }

            CSData.SaveChanges();
            return RedirectToAction("AccountList");
        }

	}
}