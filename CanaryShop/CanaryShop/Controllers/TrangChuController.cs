﻿using Models.Frameworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CanaryShop.Controllers
{
    public class TrangChuController : Controller
    {
        OnlineShopDbContext db = new OnlineShopDbContext();
        public ActionResult TrangChu()
        {
            string sChuoi = "";
            //Lay danh sach san pham tu database
            var spham = (from p in db.SANPHAMQAs orderby p.MaSP descending select p).Take(8).ToList();
            //Duyet danh sach san pham
            for (int i = 0; i < spham.Count;i++ )
            {
                sChuoi += "<li class=\"span3\">";
                     sChuoi += "<div class=\"product-box\">";
                     sChuoi += "<span class=\"sale_tag\"></span>";
                     sChuoi += "<p><a href=\"/SanPham/ChiTietSanPham/11\"><img alt=\"\" src=\"" + spham[i].HinhAnh + "\"></a></p>";
                     sChuoi += "<a href=\"/SanPham/ChiTietSanPham\">" + spham[i].TenSP + "</a><br />";
                     sChuoi += "<p class=\"price\">"+spham[i].GiaBan+"</p>";
                     sChuoi += "</div>";
                sChuoi += "</li>";
            }
            ViewBag.View = sChuoi;
             return View();
        }

        public ActionResult ThongTin()
        {
            return View();
        }

        public ActionResult LienHe()
        {
            return View();
        }
    }
}