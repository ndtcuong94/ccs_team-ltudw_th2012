﻿using CanaryShop.Models;
using Models.Frameworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CanaryShop.Controllers
{
    public class MuaHangController : Controller
    {
        private OnlineShopDbContext dbContext = new OnlineShopDbContext();

        //hien thi danh sach cac don dat hang
        // GET: MuaHang
        public ActionResult Index()
        {
            return View(dbContext);
        }

        #region Thong tin don hang
        // GET: MuaHang/Details/5
        public ActionResult Details(int id)
        {
            return View(dbContext.GIOHANGs.Find(id));
        }
        #endregion

        #region Create gio hang
        // GET: MuaHang/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MuaHang/Create
        [HttpPost]
        public ActionResult Create([Bind(Exclude = "OrderID")] GIOHANG model)
        {
            var CartList = (List<CartVM>)Session["Cart"];
            var OrdetailsList = new List<OrderDetail>();
            CartList.ForEach(m =>
            {
                var _Product = dbContext.SANPHAMQAs.Find(m.Sanpham.MaSP);
                OrdetailsList.Add(new OrderDetail
                {
                    MaSP = _Product.MaSP,
                    SoLuong = m.SoLuong
                });
            });
            model.TrangThai = false;
            // Thêm danh sách OrderDetails
            model.OrderDetails = OrdetailsList;

            // Thêm bảng Order
            dbContext.GIOHANGs.Add(model);
            dbContext.SaveChanges();
            Session["Cart"] = null;
            return RedirectToAction("CreateSuccess");
        }

        public ActionResult CreateSuccess()
        {
            return View();
        }

        #endregion


        // GET: MuaHang/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: MuaHang/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        #region Delete don hang

        // POST: MuaHang/Delete/5
        [HttpPost]
        public ActionResult Delete(int id)
        {
            var _Order = dbContext.GIOHANGs.Find(id);
            _Order.OrderDetails.ToList().ForEach(m => dbContext.OrderDetails.Remove(m));
            dbContext.GIOHANGs.Remove(_Order);
            dbContext.SaveChanges();
            return new EmptyResult();
        }

        #endregion

        #region Gio hang
        public ActionResult ShowCart()
        {
            return View();
        }

        public ActionResult AddToCart(int id)
        {
            var _Product = dbContext.SANPHAMQAs.Find(id);
            var CartList = new List<CartVM>();
            if (Session["Cart"] != null)
            {
                CartList = (List<CartVM>)Session["Cart"];
                var OldCart = CartList.Find(m => m.Sanpham.MaSP == id);
                if (OldCart != null)
                {
                    var NewCart = new CartVM { Sanpham = _Product, SoLuong = OldCart.SoLuong + 1 };
                    CartList.Remove(OldCart);
                    CartList.Add(NewCart);
                }
                else
                {
                    CartList.Add(new CartVM { Sanpham = _Product, SoLuong = 1 });
                }
            }
            else
            {
                CartList.Add(new CartVM { Sanpham = _Product, SoLuong = 1 });
            }
            Session["Cart"] = CartList;
            return RedirectToAction("ShowCart");
        }

        public ActionResult RemoveCart(int id)
        {
            var CartList = (List<CartVM>)Session["Cart"];
            var _Cart = CartList.SingleOrDefault(m => m.Sanpham.MaSP == id);
            CartList.Remove(_Cart);
            Session["Cart"] = CartList;
            return new EmptyResult();
        }
        #endregion
    }
}
