﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace CanaryShop.Code
{
        public class RecaptchaResult
        {
            [JsonProperty("success")]
            public bool Susscess { get; set; }

            [JsonProperty("error-codes")]
            public List<string> ErrorCodes { get; set; }
        }

}