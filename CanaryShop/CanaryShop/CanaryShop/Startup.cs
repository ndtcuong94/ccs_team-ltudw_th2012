﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CanaryShop.Startup))]
namespace CanaryShop
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
