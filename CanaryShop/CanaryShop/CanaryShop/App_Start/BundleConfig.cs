﻿using System.Web;
using System.Web.Optimization;

namespace CanaryShop
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/common.js",
                        "~/Scripts/jquery.flexslider-min.js",
                        "~/Scripts/jquery.fancybox.js",
                        "~/Scripts/jquery.scrolltotop.js",
                        "~/Scripts/jquery-1.7.2.min.js",
                        "~/Scripts/superfish.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js",
                      "~/Scripts/bootstrap1.js",
                      "~/Scripts/bootstrap1.min.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css",
                      "~/Content/bootstrap1.css",
                      "~/Content/bootstrappage.css",
                      "~/Content/bootstrap-responsive.css",
                      "~/Content/bootstrap-responsive.min.css",
                      "~/Content/flexslider.css",
                      "~/Content/jquery.fancybox.css",
                      "~/Content/main.css",
                      "~/Content/style.css"));
        }
    }
}
