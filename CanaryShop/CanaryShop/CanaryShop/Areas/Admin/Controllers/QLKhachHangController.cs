﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Models.Frameworks;
using Models;
using PagedList;
using System.Data.Entity.Infrastructure;
namespace CanaryShop.Areas.Admin.Controllers
{
    public class QLKhachHangController : Controller
    {
        private OnlineShopDbContext dbContext = new OnlineShopDbContext();

        // GET: Admin/QLKhachHang
        //Hiển thị danh sách các khách hàng
        public ActionResult Index()
        {
            var dsKH = new QLNguoiDungModel();
            var model = dsKH.ListAll();
            return View(model);
        }

        // GET: Admin/QLKhachHang/Details/5
        public ActionResult Details(int id)
        {
            return View(dbContext.TAIKHOANs.First(c => c.MaKH == id));
        }

        // GET: Admin/QLKhachHang/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/QLKhachHang/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/QLKhachHang/Edit/5
        public ActionResult Edit(int id)
        {
            return View(dbContext.TAIKHOANs.First(c=>c.MaKH == id));
        }

        // POST: Admin/QLKhachHang/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, TAIKHOAN taikhoan)
        {
            try
            {
                // TODO: Add update logic here
                if(ModelState.IsValid)
                {
                    TAIKHOAN tk = dbContext.TAIKHOANs.First(c => c.MaKH == id);
                    ((IObjectContextAdapter)dbContext).ObjectContext.ApplyCurrentValues("TAIKHOANs", taikhoan);
                    dbContext.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(taikhoan);
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/QLKhachHang/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Admin/QLKhachHang/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
