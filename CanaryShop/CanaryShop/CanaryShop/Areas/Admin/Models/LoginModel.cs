﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CanaryShop.Areas.Admin.Models
{
    public class LoginModel
    {
        [Required]
        [Display(Name = "Tên đăng nhập:")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Mật khẩu")]
        public string Password { get; set; }

        [Display(Name = "Bạn quên mật khẩu?")]
        public bool RememberMe { get; set; }
    }
}