﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Models;
using Models.Frameworks;
using PagedList;
using System.Data.Entity;

namespace CanaryShop.Controllers
{
    public class SanPhamController : Controller
    {
        private OnlineShopDbContext dbContext = new OnlineShopDbContext();
        // GET: /Product/
        
        public ActionResult AoKhoacNu()
        {
            string sChuoi = "";
            //Lay danh sach san pham tu database
            var spham = (from p in dbContext.SANPHAMQAs orderby p.IDLoaiSP==1 select p).Take(8).ToList();
            //Duyet danh sach san pham
            for (int i = 0; i < spham.Count; i++)
            {
                sChuoi += "<li class=\"span3\">";
                sChuoi += "<div class=\"product-box\">";
                sChuoi += "<span class=\"sale_tag\"></span>";
                sChuoi += "<p><a href=\"/SanPham/ChiTietSanPham/5\"><img alt=\"\" src=\"" + spham[i].HinhAnh + "\"></a></p>";
                sChuoi += "<a href=\"/SanPham/ChiTietSanPham/5\">" + spham[i].TenSP + "</a><br />";
                sChuoi += "<p class=\"price\">" + spham[i].GiaBan + "</p>";
                sChuoi += "</div>";
                sChuoi += "</li>";
            }
            ViewBag.View = sChuoi;
            return View();
        }

        public ActionResult AoKhoacNam()
        {
            string sChuoi = "";
            //Lay danh sach san pham tu database
            var spham = (from p in dbContext.SANPHAMQAs orderby p.IDLoaiSP==2 select p).Take(8).ToList();
            //Duyet danh sach san pham
            for (int i = 0; i < spham.Count; i++)
            {
                sChuoi += "<li class=\"span3\">";
                sChuoi += "<div class=\"product-box\">";
                sChuoi += "<span class=\"sale_tag\"></span>";
                sChuoi += "<p><a href=\"/SanPham/ChiTietSanPham/11\"><img alt=\"\" src=\"" + spham[i].HinhAnh + "\"></a></p>";
                sChuoi += "<a href=\"/SanPham/ChiTietSanPham/11\">" + spham[i].TenSP + "</a><br />";
                sChuoi += "<p class=\"price\">" + spham[i].GiaBan + "</p>";
                sChuoi += "</div>";
                sChuoi += "</li>";
            }
            ViewBag.View = sChuoi;
            return View();
        }

        //Danh sach san pham co trong du lieu
        public ActionResult List(int ?page)
        {
            var sanpham = from s in dbContext.SANPHAMQAs
                           orderby s.MaSP
                           select s;

            int pageSize = 3;
            int pageNumber = (page ?? 1);
            return View(sanpham.ToPagedList(pageNumber, pageSize));
        }

        //Hien thi danh sach san pham
        public ViewResult Index( )
        {
            return View(dbContext.LOAISANPHAMs);
        }

        public ActionResult SanPhamID(int id)
        {
            return View(dbContext.SANPHAMQAs.Where(m => m.IDLoaiSP == id));
        }

        public ActionResult ChiTietSanPham(int id)
        {
            return View(dbContext.SANPHAMQAs.Find(id));
        }

        #region Create
        [HttpPost, Authorize]
        public ActionResult TaoMoiSP()
        {
            ViewBag.IDLoaiSP = new SelectList(dbContext.LOAISANPHAMs, "IDLoaiSP", "Ten");
            return View();
            }
        [HttpPost]
        public ActionResult TaoMoiSP([Bind(Exclude="IDLoaiSP")] SANPHAMQA model)
        {
            if(ModelState.IsValid)
            {
                dbContext.SANPHAMQAs.Add(model);
                dbContext.SaveChanges();
                return RedirectToAction("ChiTiet", new { id = model.MaSP });
            }
            ViewBag.IDLoaiSP = new SelectList(dbContext.LOAISANPHAMs, "IDLoaiSP", "Ten", model.IDLoaiSP);
            return View(model);
        }
        #endregion

        #region Edit
        public ActionResult ChinhSua(int id)
        {
            var sanpham = dbContext.SANPHAMQAs.Find(id);
            ViewBag.IDLoaiSP = new SelectList(dbContext.LOAISANPHAMs, "IDLoaiSP", "Ten", sanpham.IDLoaiSP);
            return View(sanpham);
        }

        [HttpPost]
        public ActionResult ChinhSua(SANPHAMQA model)
        {
            if(ModelState.IsValid)
            {
                dbContext.Entry(model).State = EntityState.Modified;
                dbContext.SaveChanges();
                return RedirectToAction("ChiTiet", new { id = model.MaSP });
            }
            ViewBag.IDLoaiSP = new SelectList(dbContext.LOAISANPHAMs, "IDLoaiSP", "Ten", model.IDLoaiSP);
            return View(model);
        }
    
        #endregion

        #region Delete
        [HttpPost]
        public ActionResult Xoa(int id)
        {
            var sanpham = dbContext.SANPHAMQAs.Find(id);
            dbContext.SANPHAMQAs.Remove(sanpham);
            dbContext.SaveChanges();
            return new EmptyResult();
        }

        #endregion
    }
}