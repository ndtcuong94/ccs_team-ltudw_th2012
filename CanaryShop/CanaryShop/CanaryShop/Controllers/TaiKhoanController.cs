﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CanaryShop.Models;
using PagedList;
using Models;
using CanaryShop.Code;
using System.Web.Security;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;
using Models.Frameworks;
using Microsoft.Web.Helpers;
using System.Net;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;

namespace CanaryShop.Controllers
{
    public class TaiKhoanController : Controller
    {
        private OnlineShopDbContext dbContext = new OnlineShopDbContext();

        public ActionResult Index()
        {
            var dsKH = new QLNguoiDungModel();
            var model = dsKH.ThongTinKH();
            return View(model);
        }
        public ActionResult Details(int id)
        {
            return View(dbContext.TAIKHOANs.First(c => c.MaKH == id));
        }

        #region ChinhSua(Edit)
        // GET: Admin/QLKhachHang/Edit/5
        public ActionResult Edit(int id)
        {
            return View(dbContext.TAIKHOANs.First(c => c.MaKH == id));
        }

        // POST: Admin/QLKhachHang/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, TAIKHOAN taikhoan)
        {
            try
            {
                // TODO: Add update logic here
                if (ModelState.IsValid)
                {
                    TAIKHOAN tk = dbContext.TAIKHOANs.First(c => c.MaKH == id);
                    ((IObjectContextAdapter)dbContext).ObjectContext.ApplyCurrentValues("TAIKHOANs", taikhoan);
                    dbContext.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(taikhoan);
            }
            catch
            {
                return View();
            }
        }
        #endregion

        #region DangNhap
        // GET: /Account/
        [HttpGet]
        public ActionResult DangNhap()
        {
            return View();
        }
        [HttpPost]
        public ActionResult DangNhap(LoginViewModel model)
        {
            var result = new AccountModel().LoginKH(model.UserName, model.Password);
            if (result && ModelState.IsValid)
            {
                SessionHelper.SetSession(new UserSession() { UserName = model.UserName });
                Session["user"] = (new UserSession() { UserName = model.UserName });

                return RedirectToAction("TrangChu", "TrangChu");
            }
            else
            {
                ModelState.AddModelError("", "Tên đăng nhập hoặc mật khẩu không đúng");
            }
            return View(model);

        }

        #endregion

        #region DangXuat
        public ActionResult DangXuat()
        {
            Session.Clear();
            return RedirectToAction("TrangChu", "TrangChu");
        }
        #endregion

        #region DangKi
        public ActionResult DangKy()
        {
            return View();
        }

        // POST: /Account/Register

        [HttpPost]
        [ValidateAntiForgeryToken]

        //public async Task<ActionResult> DangKy(TAIKHOAN model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return View(model);
        //    }
        //    var userStore = new UserStore<IdentityUser>();
        //    var userManager = new UserManager<IdentityUser>(userStore);

        //    var user = await userManager.FindAsync(model.TenDangNhap, model.MatKhau);

        //    if (user != null)
        //    {
        //        ModelState.AddModelError(string.Empty, "User da ton tai");
        //    }

        //    user = new IdentityUser { UserName = model.TenDangNhap, Email = model.Email };

        //    await userManager.CreateAsync(user, model.MatKhau);

        //    return RedirectToAction("TrangChu", "TrangChu");
        //}
        public ActionResult DangKy(TAIKHOAN model)
        {
                var response = Request["g-recaptcha-respone"];

                const string secret = "6LeFrAgTAAAAAEYM8XjWB0fhC2CNHhpTtfXEYCET";

                var client = new WebClient();
                var reply = client.DownloadString(
                    string.Format("https://www.google.com/recaptcha/api/siteverify?secret={1}&response={1}", secret, response));
                var captchaResponse = JsonConvert.DeserializeObject<RecaptchaResult>(reply);

                //Cac loi khi nhap sai captcha
                if (!captchaResponse.Susscess)
                {
                    if (ModelState.IsValid)
                    {
                        using (OnlineShopDbContext dbContext = new OnlineShopDbContext())
                        {
                            dbContext.TAIKHOANs.Add(model);
                            dbContext.SaveChanges();
                            ModelState.Clear();
                            model = null;
                            ViewBag.Message = "Đăng kí thành công";
                        }
                    }
                }
                else
                {
                    if (captchaResponse.ErrorCodes.Count <= 0)
                        return View();

                    var error = captchaResponse.ErrorCodes[0].ToLower();
                    switch (error)
                    {
                        case ("missing-input-secret"):
                            ViewBag.Message = "The secret parameter is missing.";
                            break;
                        case ("invalid-input-secret"):
                            ViewBag.Message = "The secret parameter is invalid or malformed.";
                            break;

                        case ("missing-input-response"):
                            ViewBag.Message = "The response parameter is missing.";
                            break;
                        case ("invalid-input-response"):
                            ViewBag.Message = "The response parameter is invalid or malformed.";
                            break;

                        default:
                            ViewBag.Message = "Error occured. Please try again";
                            break;
                    }
                }
                return View(model);
        }

        #endregion
    }
}
