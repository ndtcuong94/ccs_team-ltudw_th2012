﻿using Models.Frameworks;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CanaryShop.Controllers
{
    public class LoaiSanPhamController : Controller
    {
        private OnlineShopDbContext dbContext = new OnlineShopDbContext();
        // GET: LoaiSanPham
        public ActionResult Index()
        {
            return View(dbContext.LOAISANPHAMs);
        }

        // GET: LoaiSanPham/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        #region Create
        // GET: LoaiSanPham/Create
        //[HttpGet]
        //public ActionResult Create()
        //{
        //    return View();
        //}

        //// POST: LoaiSanPham/Create
        //[HttpPost]
        //public ActionResult Create([Bind(Exclude = "IDLoaiSP")] LOAISANPHAM model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        dbContext.LOAISANPHAMs.Add(model);
        //        dbContext.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    return View(model);
        //}

        #endregion

        #region Edit
        //// GET: LoaiSanPham/Edit/5
        //[HttpGet]
        //public ActionResult Edit(int id)
        //{
        //    return View(dbContext.LOAISANPHAMs.Find(id));
        //}

        //// POST: LoaiSanPham/Edit/5
        //[HttpPost]
        //public ActionResult Edit(LOAISANPHAM model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        dbContext.Entry(model).State = EntityState.Modified;
        //        dbContext.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    return View(model);
        //}
        #endregion

        #region Delete
        // POST: LoaiSanPham/Delete/5
        //[HttpPost]
        //public ActionResult Delete(int id)
        //{
        //    var _Category = dbContext.LOAISANPHAMs.Find(id);
        //    _Category.SANPHAMQAs.ToList().ForEach(m => dbContext.SANPHAMQAs.Remove(m));
        //    dbContext.LOAISANPHAMs.Remove(_Category);
        //    dbContext.SaveChanges();
        //    return new EmptyResult();
        //}

        #endregion
    }
}
