﻿using Models.Frameworks;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Models
{
   public class QLNguoiDungModel
    {
        private OnlineShopDbContext context = null;
        
        public QLNguoiDungModel()
        {
            context = new OnlineShopDbContext();
        }
        public List<TAIKHOAN> ListAll()
        {
            var list = context.Database.SqlQuery<TAIKHOAN>("sp_User_GetByAll").ToList();
            return list;
        }

       public List<TAIKHOAN> ThongTinKH()
        {
            var list = context.Database.SqlQuery<TAIKHOAN>("sp_User_GetByMaKH").ToList();
            return list;
        }
        public int Create(string TenDangNhap, string MatKhau, string HoTen, string GioiTinh, string DiaChi, string Email, string Sdt)
        {
            object[] para = 
            {
                new SqlParameter("@TenDangNhap", TenDangNhap),
                new SqlParameter("@MatKhau", MatKhau),
                new SqlParameter("@HoTen", HoTen),
                new SqlParameter("@GioiTinh", GioiTinh),
                new SqlParameter("@DiaChi", DiaChi),
                new SqlParameter("@Email", Email),
                new SqlParameter("@SDT", Sdt)
            };
            int res = context.Database.ExecuteSqlCommand("sp_User_Insert @TenDangNhap, @MatKhau, @HoTen, @GioiTinh, @DiaChi, @Email, @SDT", para);
            return res;
        }
    }
}

