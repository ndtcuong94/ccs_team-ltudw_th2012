﻿using Models.Frameworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CanaryShop.Models
{
    public class CartVM
    {
        public SANPHAMQA Sanpham { get; set; }

        public int SoLuong { get; set; }
    }
}