namespace Models.Frameworks
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SANPHAMQA")]
    public partial class SANPHAMQA
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MaSP { get; set; }

        [StringLength(50)]
        public string TenSP { get; set; }

        public int? IDLoaiSP { get; set; }

        [StringLength(250)]
        public string HinhAnh { get; set; }

        public decimal? GiaBan { get; set; }

        public DateTime? NgayTao { get; set; }

        [Column(TypeName = "ntext")]
        public string ChiTiet { get; set; }

        public bool? TrangThai { get; set; }

        [DataType(DataType.MultilineText)]
        public string Mota { get; set; }
    }
}
