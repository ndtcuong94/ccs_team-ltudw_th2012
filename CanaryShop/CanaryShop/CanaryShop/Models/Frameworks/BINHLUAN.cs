namespace Models.Frameworks
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BINHLUAN")]
    public partial class BINHLUAN
    {
        [Key]
        [StringLength(50)]
        public string Ten { get; set; }

        public int? MaBaiViet { get; set; }

        [StringLength(200)]
        public string NoiDung { get; set; }
    }
}
