namespace Models.Frameworks
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("GIOHANG")]
    public partial class GIOHANG
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int OrderID { get; set; }

        [StringLength(50)]
        public string KhachHang { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        [StringLength(12)]
        public string SDT { get; set; }

        [StringLength(50)]
        public string DiaChi { get; set; }

        public bool? TrangThai { get; set; }

        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
