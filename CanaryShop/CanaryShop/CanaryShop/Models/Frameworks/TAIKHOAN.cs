﻿namespace Models.Frameworks
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TAIKHOAN")]
    public partial class TAIKHOAN
    {
        [Key]
        public int MaKH { get; set; }

        [StringLength(50)]
        [Required(ErrorMessage = "Xin nhập vào tên đăng nhập.", AllowEmptyStrings = false)]
        public string TenDangNhap { get; set; }

        [StringLength(50)]
        [Required(ErrorMessage = "Xin nhập vào mật khẩu.", AllowEmptyStrings = false)]
        [DataType(System.ComponentModel.DataAnnotations.DataType.Password)]
        public string MatKhau { get; set; }

        //[Compare("MatKhau", ErrorMessage = "Mật khẩu nhập vào không khớp.")]
        //[DataType(System.ComponentModel.DataAnnotations.DataType.Password)]
        //public string ConfirmPassword { get; set; }

        [StringLength(50)]
        [Required(ErrorMessage = "Xin nhập vào họ tên đầy đủ.", AllowEmptyStrings = false)]
        public string HoTen { get; set; }

        [StringLength(7)]
        [Required(ErrorMessage = "Xin chọn giới tính.", AllowEmptyStrings = false)]
        public string GioiTinh { get; set; }

        [StringLength(50)]
        [Required(ErrorMessage = "Xin nhập vào địa chỉ của bạn.", AllowEmptyStrings = false)]
        public string DiaChi { get; set; }

        [StringLength(50)]
        [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*",
            ErrorMessage = "Xin nhập đúng định dạng email: Ví dụ: a@gmail.com")]
        public string Email { get; set; }

        [StringLength(12)]
        public string SDT { get; set; }
    }
}
