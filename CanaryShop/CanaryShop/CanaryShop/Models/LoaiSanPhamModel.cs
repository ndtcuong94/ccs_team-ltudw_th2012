﻿using Models.Frameworks;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Models
{
    public class LoaiSanPhamModel
    {
        private OnlineShopDbContext context = null;

        public LoaiSanPhamModel()
        {
            context = new OnlineShopDbContext();
        }

        public List<LOAISANPHAM> ListAll()
        {
            var list = context.Database.SqlQuery<LOAISANPHAM>("Sp_LoaiSanPham_ListAll").ToList();
            return list;
        }

        public int Create(string ten, int? order, bool? trangthai)
        {
            object[] para =
            {
                new SqlParameter("@Ten", ten),
                new SqlParameter("@Order",order),
                new SqlParameter("@TrangThai",trangthai)
            };
            int res = context.Database.ExecuteSqlCommand("Sp_LoaiSanPham_Insert @Ten, @Order, @TrangThai", para);
            return res;
        }
    }
}
