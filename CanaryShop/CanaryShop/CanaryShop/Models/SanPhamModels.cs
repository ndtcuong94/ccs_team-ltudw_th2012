﻿using Models.Frameworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Models
{
    public class SanPhamModels
    {
       private OnlineShopDbContext context = null;

        public SanPhamModels()
        {
            context = new OnlineShopDbContext();
        }

        public class SanPhamPTrang
        {
            private static readonly IList<SANPHAMQA> spham = new List<SANPHAMQA>();

            public SanPhamPTrang()
            {
                if(spham.Count == 0)
                {
                    for(var i=1;i<100;i++)
                    {
                        var p = new SANPHAMQA() { MaSP = i, TenSP = Guid.NewGuid().ToString() };
                        spham.Add(p);
                    }
                }
            }

            //Lay du lieu theo trang. tra ve tong so ban ghi co trong du lieu
            public IList<SANPHAMQA> Get(int page, out int total)
            {
                var sanpham = (from p in spham select p).Skip((page - 1) * 10).Take(10);
                var totalSanpham = (from p in spham select p).Count();
                total = totalSanpham / 10 + (totalSanpham % 10 > 0 ? 1 : 0);
                return sanpham.ToList();    
            }

            //Tim kiem va tra ket qua dua tren chi so trang va tieu chi tim kiem
            public IList<SANPHAMQA> Get(int page, string criteria, out int total)
            {
                var sanphams = (from p in spham where p.TenSP.StartsWith(criteria) select p).Skip((page - 1) * 10).Take(10);
                var totalSanpham = (from p in spham select p).Count();
                total = totalSanpham / 10 + (totalSanpham % 10 > 0 ? 1 : 0);
                return sanphams.ToList();
            }
        }
    }
}
