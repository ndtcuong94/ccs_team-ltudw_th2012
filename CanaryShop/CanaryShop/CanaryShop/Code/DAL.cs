﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Models.Frameworks;
namespace CanaryShop.Code
{
    public class DAL
    {
        static SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["OnlineShopDbContext"].ToString());

        public static bool UserIsValid(string userName, string passWord)
        {
            bool authenticated = false;

            string query = string.Format("SELECT * FROM [TAIKHOAN] WHERE TenDangNhap = '{0}' AND MatKhau= '{1}'", userName, passWord);

            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            SqlDataReader sdr = cmd.ExecuteReader();
            authenticated = sdr.HasRows;
            conn.Close();
            return (authenticated);
        }
    }
}