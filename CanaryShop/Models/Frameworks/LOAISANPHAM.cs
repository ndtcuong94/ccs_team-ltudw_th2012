namespace Models.Frameworks
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("LOAISANPHAM")]
    public partial class LOAISANPHAM
    {
        [Key]
        public int IDLoaiSP { get; set; }

        [StringLength(50)]
        public string Ten { get; set; }

        public DateTime? NgayTao { get; set; }

        public int? Order { get; set; }

        public bool? TrangThai { get; set; }

        public virtual ICollection<SANPHAMQA> SANPHAMQAs { get; set; }
    }
}
