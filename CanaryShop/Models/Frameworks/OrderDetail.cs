namespace Models.Frameworks
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OrderDetail")]
    public partial class OrderDetail
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID { get; set; }

        public int? OrderID { get; set; }

        public int? MaSP { get; set; }

        public int? SoLuong { get; set; }

        public virtual SANPHAMQA Sanpham { get; set; }
        public virtual GIOHANG Giohang { get; set; }
    }
}
