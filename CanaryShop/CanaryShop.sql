USE [master]
GO
if db_id('CanaryShop') is not null 
	drop database SeaSeane;
go
Create Database CanaryShop;
go
Use CanaryShop;
go

--B?ng t�i kho?n ng??i d�ng
create table TAIKHOAN
(
	TenDangNhap varchar(20) not null primary key,
	MatKhau varchar(20) not null,
	HoTen nvarchar(50) not null,
	NgaySinh datetime not null,
	Email varchar(50),
	DiaChi nvarchar(50),
	DienThoai varchar(11),
	PhanQuyen nvarchar(20) not null
)

/****** Object:  Table [dbo].[BAIVIET]    Script Date: 5/12/2015 8:12:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BAIVIET](
	[MaBaiViet] [int] NOT NULL,
	[TieuDe] [nvarchar](100) NULL,
	[NoiDung] [nvarchar](400) NULL,
 CONSTRAINT [PK_BAIVIET] PRIMARY KEY CLUSTERED 
(
	[MaBaiViet] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BINHLUAN]    Script Date: 5/12/2015 8:12:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BINHLUAN](
	[Ten] [nvarchar](50) NOT NULL,
	[MaBaiViet] [int] NULL,
	[NoiDung] [nvarchar](200) NULL,
 CONSTRAINT [PK_BINHLUAN] PRIMARY KEY CLUSTERED 
(
	[Ten] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]